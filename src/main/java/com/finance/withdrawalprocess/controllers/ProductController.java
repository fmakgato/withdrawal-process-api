package com.finance.withdrawalprocess.controllers;

import com.finance.withdrawalprocess.entity.Product;
import com.finance.withdrawalprocess.models.ProcessRequest;
import com.finance.withdrawalprocess.services.ProductService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("product")
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @ApiOperation(value = "Linking a product to an investor and save onto the DB")
    @PutMapping("/link")
    public ResponseEntity<Product> linkProductToInvestor(@RequestBody ProcessRequest processRequest) {
        return ResponseEntity.ok(this.productService.linkProductToInvestor(processRequest));
    }

    @ApiOperation(value = "Creating a new product and save onto the DB")
    @PostMapping("/")
    public ResponseEntity<Product> createNewProduct(@RequestBody Product product) {
        return ResponseEntity.ok(this.productService.createNewProduct(product));
    }

    @ApiOperation(value = "Retrieving a list of products linked to a certain investor the DB")
    @GetMapping("/{investorId}")
    public ResponseEntity<List<Product>> getProductsForInvestor(@PathVariable long investorId) {
        return ResponseEntity.ok(this.productService.getProductsForInvestor(investorId));
    }
}
