package com.finance.withdrawalprocess.controllers;

import com.finance.withdrawalprocess.entity.Withdrawal;
import com.finance.withdrawalprocess.models.ProcessRequest;
import com.finance.withdrawalprocess.models.WithdrawalRequest;
import com.finance.withdrawalprocess.services.WithdrawalService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("withdrawal")
public class WithdrawalController {
    // Documentation - http://localhost:8092/v3/api-docs
    // Doc swagger-ui - http://localhost:8092/swagger-ui/
    // DB Console - http://localhost:8092/h2-console
    /*
    SELECT * FROM INVESTOR;
    SELECT * FROM PRODUCT;
    SELECT * FROM USER_ROLE;
    SELECT * FROM WITHDRAWAL;
    */

    private final WithdrawalService withdrawalService;

    @Autowired
    public WithdrawalController(WithdrawalService withdrawalService) {
        this.withdrawalService = withdrawalService;
    }

    @ApiOperation(value = "Creating a new withdrawal and save onto the DB")
    @PostMapping("/")
    public ResponseEntity<Withdrawal> createNewWithdrawal(@RequestBody WithdrawalRequest request) {
        Withdrawal withdrawalResponse = this.withdrawalService.createNewWithdrawal(request);
        return ResponseEntity.ok(withdrawalResponse);
    }

    @ApiOperation(value = "Processing a withdrawal and save onto the DB")
    @PutMapping("/process")
    public ResponseEntity<Withdrawal> processWithdrawal(@RequestBody ProcessRequest request) {
        Withdrawal withdrawalResponse = this.withdrawalService.processWithdrawal(request);

        return ResponseEntity.ok(withdrawalResponse);
    }
}
