package com.finance.withdrawalprocess.controllers;

import com.finance.withdrawalprocess.entity.Investor;
import com.finance.withdrawalprocess.services.InvestorService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("investor")
public class InvestorController {

    private final InvestorService investorService;

    @Autowired
    public InvestorController(InvestorService investorService) {
        this.investorService = investorService;
    }

    @ApiOperation(value = "Retrieving investor information from the DB")
    @GetMapping("/{investorId}")
    public ResponseEntity<Investor> retrieveInvestorInformation(@PathVariable long investorId) {
        Investor investor = this.investorService.retrieveInvestorInformation(investorId);
        return ResponseEntity.ok(investor);
    }

    @ApiOperation(value = "Creating a new investor and save onto the DB")
    @PostMapping("/")
    public ResponseEntity<Investor> createNewInvestor(@RequestBody Investor investor) {
        return ResponseEntity.ok(this.investorService.createNewInvestor(investor));
    }
}
