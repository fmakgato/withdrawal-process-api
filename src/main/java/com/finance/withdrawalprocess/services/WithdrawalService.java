package com.finance.withdrawalprocess.services;

import com.finance.withdrawalprocess.entity.Withdrawal;
import com.finance.withdrawalprocess.models.ProcessRequest;
import com.finance.withdrawalprocess.models.WithdrawalRequest;

public interface WithdrawalService {

    Withdrawal createNewWithdrawal(WithdrawalRequest request);

    Withdrawal processWithdrawal(ProcessRequest request);
}
