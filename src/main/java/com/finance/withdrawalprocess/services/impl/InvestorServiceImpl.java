package com.finance.withdrawalprocess.services.impl;

import com.finance.withdrawalprocess.configurations.PasswordEncoderConfig;
import com.finance.withdrawalprocess.entity.Investor;
import com.finance.withdrawalprocess.enums.Role;
import com.finance.withdrawalprocess.repository.InvestorRepository;
import com.finance.withdrawalprocess.services.InvestorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InvestorServiceImpl implements InvestorService {

    private final InvestorRepository investorRepository;
    private final PasswordEncoderConfig passwordEncoderConfig;

    @Autowired
    public InvestorServiceImpl(InvestorRepository investorRepository, PasswordEncoderConfig passwordEncoderConfig) {
        this.investorRepository = investorRepository;
        this.passwordEncoderConfig = passwordEncoderConfig;
    }

    @Override
    public Investor retrieveInvestorInformation(long investorId) {
        Investor investor;

        try {
            investor = this.investorRepository.findByInvestorId(investorId);
        } catch (Exception e) {
            throw new RuntimeException(String.format("Investor details not found for investor id: %d", investorId));
        }

        return investor;
    }

    @Override
    public Investor createNewInvestor(Investor investor) {
        Investor newInvestor;
        investor.setPassword(passwordEncoderConfig.bCryptPasswordEncoder().encode(investor.getPassword()));

        try {
            if (investor.getUserRole().getRoleName() == null) {
                investor.getUserRole().setRoleName(Role.ADMIN_ROLE.toString());
            }

            newInvestor = this.investorRepository.save(investor);
        } catch (Exception e) {
            throw new RuntimeException(String.format("Unable to create new investor with email: %s", investor.getEmailAddress()));
        }

        return newInvestor;
    }
}
