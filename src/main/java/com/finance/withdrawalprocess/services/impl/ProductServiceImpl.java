package com.finance.withdrawalprocess.services.impl;

import com.finance.withdrawalprocess.entity.Investor;
import com.finance.withdrawalprocess.entity.Product;
import com.finance.withdrawalprocess.models.ProcessRequest;
import com.finance.withdrawalprocess.repository.InvestorRepository;
import com.finance.withdrawalprocess.repository.ProductRepository;
import com.finance.withdrawalprocess.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final InvestorRepository investorRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository, InvestorRepository investorRepository) {
        this.productRepository = productRepository;
        this.investorRepository = investorRepository;
    }

    @Override
    public List<Product> getProductsForInvestor(long investorId) {
        List<Product> products;

        try {
            products = this.productRepository.findAllByInvestor_InvestorId(investorId);
        } catch (Exception e) {
            throw new RuntimeException(String.format("Unable to get investor products for investorId: %d", investorId));
        }

        return products;
    }

    @Override
    public Product linkProductToInvestor(ProcessRequest processRequest) {
        Product product;
        Investor investor;
        Product investorProductLink;

        try {
            product = this.productRepository.findByProductID(processRequest.getProductId());
            investor = this.investorRepository.findByInvestorId(processRequest.getInvestorId());

            product.setInvestor(investor);

            investorProductLink = this.productRepository.save(product);
        } catch (Exception e) {
            throw new RuntimeException(String.format("Unable to link product to investor id: %d", processRequest.getProductId()));
        }

        return investorProductLink;
    }

    @Override
    public Product createNewProduct(Product product) {
        Product newProduct;

        try {
            newProduct = this.productRepository.save(product);
        } catch (Exception e) {
            throw new RuntimeException(String.format("Unable to create product with name %s", product.getProductName()));
        }

        return newProduct;
    }
}
