package com.finance.withdrawalprocess.services.impl;

import com.finance.withdrawalprocess.entity.Investor;
import com.finance.withdrawalprocess.entity.Product;
import com.finance.withdrawalprocess.entity.Withdrawal;
import com.finance.withdrawalprocess.enums.ProductType;
import com.finance.withdrawalprocess.enums.WithdrawalStatus;
import com.finance.withdrawalprocess.models.ProcessRequest;
import com.finance.withdrawalprocess.models.WithdrawalRequest;
import com.finance.withdrawalprocess.repository.InvestorRepository;
import com.finance.withdrawalprocess.repository.ProductRepository;
import com.finance.withdrawalprocess.repository.WithdrawalRepository;
import com.finance.withdrawalprocess.services.WithdrawalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.Objects;

@Service
public class WithdrawalServiceImpl implements WithdrawalService {

    private final WithdrawalRepository withdrawalRepository;
    private final ProductRepository productRepository;
    private final InvestorRepository investorRepository;

    @Autowired
    public WithdrawalServiceImpl(
            WithdrawalRepository withdrawalRepository,
            ProductRepository productRepository,
            InvestorRepository investorRepository) {
        this.withdrawalRepository = withdrawalRepository;
        this.productRepository = productRepository;
        this.investorRepository = investorRepository;
    }

    @Override
    public Withdrawal createNewWithdrawal(WithdrawalRequest request) {

        Withdrawal withdrawalResponse;
        Withdrawal withdrawal;

        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setWithdrawalId(request.getWithdrawalId());
        processRequest.setInvestorId(request.getInvestorId());
        processRequest.setProductId(request.getProductId());
        processRequest.setWithdrawalAmount(request.getWithdrawalAmount());

        try {

            withdrawal = this.checkAndSetWithdrawal(processRequest);
            withdrawal.setBankName(request.getBankName());
            withdrawal.setAccountNumber(request.getAccountNumber());
            withdrawal.setReference(request.getReference());
            withdrawal.setBranchCode(request.getBranchCode());

            this.checkValidations(processRequest, withdrawal);

            withdrawalResponse = this.withdrawalProcessing(withdrawal, processRequest);

            return this.withdrawalRepository.save(withdrawalResponse);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public Withdrawal processWithdrawal(ProcessRequest request) {
        Withdrawal withdrawal = this.checkAndSetWithdrawal(request);

        if (!Objects.equals(withdrawal.getStatus(), WithdrawalStatus.DONE.toString())) {
            this.withdrawalProcessing(withdrawal, request);
        }

        // TODO if withdrawal status is "DONE" send notification

        return this.withdrawalProcessing(withdrawal, request);
    }

    private void checkValidations(ProcessRequest request, Withdrawal withdrawal) {

        double withdrawalLimit = (request.getWithdrawalAmount() / withdrawal.getProduct().getCurrentBalance()) * 100;

        if (request.getWithdrawalAmount() > withdrawal.getProduct().getCurrentBalance() || withdrawal.getProduct().getCurrentBalance() <= 0) {
            throw new RuntimeException(String.format("You have insufficient funds in product: %s", withdrawal.getProduct().getProductType()));
        }

        if (withdrawalLimit > 90) {
            throw new RuntimeException(String.format("Investors cannot withdraw an AMOUNT more than 90 percent of the current balance of product: %s", withdrawal.getProduct().getProductType()));
        }

        if (withdrawal.getProduct().getProductType().equals(ProductType.RETIREMENT.toString()) && (Period.between(withdrawal.getProduct().getInvestor().getDateOfBirth(), LocalDate.now()).getYears() < 65)) {
            throw new RuntimeException(String.format("Your age must be greater than 65 for product: %s", withdrawal.getProduct().getProductType()));
        }
    }

    private Withdrawal checkAndSetWithdrawal(ProcessRequest request) {
        Product product;
        Investor investor;
        Withdrawal withdrawal;

        try {
            investor = this.investorRepository.findByInvestorId(request.getInvestorId());
            product = this.productRepository.findByProductID(request.getProductId());

            if (request.getWithdrawalId() <= 0) {
                withdrawal = new Withdrawal();

                withdrawal.setWithdrawalStartDate(LocalDateTime.now());
                withdrawal.setAvailableAmountBefore(product.getCurrentBalance());
                withdrawal.setAvailableAmount(product.getCurrentBalance());
                withdrawal.setWithdrawalAmount(request.getWithdrawalAmount());
                withdrawal.setInvestorId(investor.getInvestorId());
            } else {
                withdrawal = this.withdrawalRepository.findByWithdrawalId(request.getWithdrawalId());
            }

            product.setInvestor(investor);
            withdrawal.setProduct(product);
        } catch (Exception e) {
            throw new RuntimeException(String.format("Unable to set withdrawal with withdrawal id: %d", request.getWithdrawalId()));
        }

        return withdrawal;
    }

    private Withdrawal withdrawalProcessing(Withdrawal withdrawal, ProcessRequest request) {

        // TODO add audit

        this.checkAndSetWithdrawal(request);

        if (withdrawal.getWithdrawalId() <= 0) {
            withdrawal.setStatus(WithdrawalStatus.STARTED.toString());
        }

        try {
            double newAvailableBalance = withdrawal.getProduct().getCurrentBalance() - request.getWithdrawalAmount();

            if (Objects.equals(withdrawal.getStatus(), WithdrawalStatus.STARTED.toString()) && withdrawal.getWithdrawalId() > 0) {
                withdrawal.getProduct().setCurrentBalance(newAvailableBalance);
                withdrawal.setStatus(WithdrawalStatus.EXECUTING.toString());

                this.productRepository.save(withdrawal.getProduct());
            }

            if (Objects.equals(withdrawal.getStatus(), WithdrawalStatus.EXECUTING.toString()) &&
                    (withdrawal.getProduct().getCurrentBalance() != newAvailableBalance)) {
                withdrawal.setAvailableAmount(newAvailableBalance);
                withdrawal.setStatus(WithdrawalStatus.DONE.toString());
                withdrawal.setWithdrawalProcessedDate(LocalDateTime.now());

                this.withdrawalRepository.save(withdrawal);
            }
        } catch (Exception e) {
            throw new RuntimeException(String.format("Unable to auto process withdrawal with id: %d", withdrawal.getWithdrawalId()));
        }

        return withdrawal;
    }
}
