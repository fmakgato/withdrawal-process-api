package com.finance.withdrawalprocess.services;

import com.finance.withdrawalprocess.entity.Investor;

public interface InvestorService {

    Investor retrieveInvestorInformation(long investorId);

    Investor createNewInvestor(Investor investor);
}
