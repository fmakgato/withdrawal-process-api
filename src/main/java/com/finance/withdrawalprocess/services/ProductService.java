package com.finance.withdrawalprocess.services;

import com.finance.withdrawalprocess.entity.Product;
import com.finance.withdrawalprocess.models.ProcessRequest;

import java.util.List;

public interface ProductService {

    List<Product> getProductsForInvestor(long investorId);

    Product linkProductToInvestor(ProcessRequest processRequest);

    Product createNewProduct(Product product);
}
