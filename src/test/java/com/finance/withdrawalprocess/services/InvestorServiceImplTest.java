package com.finance.withdrawalprocess.services;

import com.finance.withdrawalprocess.configurations.PasswordEncoderConfig;
import com.finance.withdrawalprocess.entity.Investor;
import com.finance.withdrawalprocess.entity.UserRole;
import com.finance.withdrawalprocess.enums.Role;
import com.finance.withdrawalprocess.repository.InvestorRepository;
import com.finance.withdrawalprocess.services.impl.InvestorServiceImpl;
import org.junit.Rule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
class InvestorServiceImplTest {

    InvestorServiceImpl investorServiceImp;
    InvestorRepository investorRepository;
    PasswordEncoderConfig passwordEncoderConfig;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @BeforeEach
    void setUp() {
        investorRepository = Mockito.mock(InvestorRepository.class);
        passwordEncoderConfig = Mockito.mock(PasswordEncoderConfig.class);
        investorServiceImp = new InvestorServiceImpl(investorRepository, passwordEncoderConfig) {
            @Override
            public Investor retrieveInvestorInformation(long investorId) {
                return null;
            }

            @Override
            public Investor createNewInvestor(Investor investor) {
                return null;
            }
        };
    }

    @Test
    void retrieveInvestorInformation() {
        when(investorRepository.findByInvestorId(0)).thenReturn(mockInvestor());

        Investor investor = investorServiceImp.retrieveInvestorInformation(0);

        Assertions.assertNotNull(investor);
        Assertions.assertEquals("Makgato", investor.getLastName());
    }

    @Test
    void retrieveInvestorInformationError() {
        expectedException.expect(RuntimeException.class);
        expectedException.expectMessage("Not found");

        when(investorRepository.findByInvestorId(0)).thenThrow(RuntimeException.class);
        investorServiceImp.retrieveInvestorInformation(1);
    }

    private Investor mockInvestor() {

        return Investor.builder()
                .investorId(1)
                .emailAddress("sfmakgato@gmail.com")
                .dateOfBirth(LocalDate.of(1921, 12, 12))
                .firstName("France")
                .mobileNumber("0764730789")
                .lastName("Makgato")
                .address("123 Mokomene 0811")
                .enabled(true)
                .locked(false)
                .password("password")
                .userRole(UserRole.builder().roleName(Role.ADMIN_ROLE.toString()).build())
                .build();
    }
}
