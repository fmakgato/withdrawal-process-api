package com.finance.withdrawalprocess.services;

import com.finance.withdrawalprocess.entity.Investor;
import com.finance.withdrawalprocess.entity.Product;
import com.finance.withdrawalprocess.entity.UserRole;
import com.finance.withdrawalprocess.enums.ProductType;
import com.finance.withdrawalprocess.enums.Role;
import com.finance.withdrawalprocess.repository.InvestorRepository;
import com.finance.withdrawalprocess.repository.ProductRepository;
import com.finance.withdrawalprocess.services.impl.ProductServiceImpl;
import org.junit.Rule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
class ProductServiceImplTest {

    ProductServiceImpl productService;
    ProductRepository productRepository;
    InvestorRepository investorRepository;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @BeforeEach
    void setUp() {
        productRepository = Mockito.mock(ProductRepository.class);
        investorRepository = Mockito.mock(InvestorRepository.class);
        productService = new ProductServiceImpl(productRepository, investorRepository);
    }

    @Test
    public void getProductsForInvestor() {
        when(productRepository.findAllByInvestor_InvestorId(1)).thenReturn(mockProducts());
        when(investorRepository.findByInvestorId(1)).thenReturn(mockInvestor());

        List<Product> products = productService.getProductsForInvestor(1);

        Assertions.assertNotNull(products);
        Assertions.assertEquals(2, products.size());
    }

    private Investor mockInvestor() {

        return Investor.builder()
                .investorId(1)
                .emailAddress("sfmakgato@gmail.com")
                .dateOfBirth(LocalDate.of(1921, 12, 12))
                .firstName("France")
                .mobileNumber("0764730789")
                .lastName("Makgato")
                .address("123 Mokomene 0811")
                .enabled(true)
                .locked(false)
                .password("password")
                .userRole(UserRole.builder().roleName(Role.ADMIN_ROLE.toString()).build())
                .build();
    }

    private List<Product> mockProducts() {

        Product savingsProduct = Product.builder()
                .productID(2)
                .productType(ProductType.SAVINGS.toString())
                .currentBalance(36000.00)
                .productName("SavingsProduct")
                .investor(mockInvestor())
                .build();

        Product retirementProduct = Product.builder()
                .productID(1)
                .productType(ProductType.RETIREMENT.toString())
                .currentBalance(500000.00)
                .productName("RetirementProduct")
                .investor(mockInvestor())
                .build();

        List<Product> products = new ArrayList<>();
        products.add(savingsProduct);
        products.add(retirementProduct);

        return products;
    }

}
