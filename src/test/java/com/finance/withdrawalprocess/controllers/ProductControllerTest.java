package com.finance.withdrawalprocess.controllers;

import com.finance.withdrawalprocess.entity.Investor;
import com.finance.withdrawalprocess.entity.Product;
import com.finance.withdrawalprocess.entity.UserRole;
import com.finance.withdrawalprocess.enums.ProductType;
import com.finance.withdrawalprocess.enums.Role;
import com.finance.withdrawalprocess.repository.ProductRepository;
import com.finance.withdrawalprocess.services.ProductService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
class ProductControllerTest {

    @Mock
    ProductService productService;

    @Mock
    ProductRepository productRepository;

    @InjectMocks
    ProductController productController;


    @Sql(executionPhase= Sql.ExecutionPhase.BEFORE_TEST_METHOD,scripts = {
            "classpath:/data/drop-tables.sql",
            "classpath:/data/create-tables.sql"})
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = {
            "classpath:/data/drop-tables.sql"})
    @BeforeEach
    void setUp() {
        productController = new ProductController(productService);
    }

    @Test
    void getProductsForInvestor() {
        Mockito.when(productService.getProductsForInvestor(1)).thenReturn(mockProducts());
        Mockito.when(productRepository.findAll()).thenReturn(mockProducts());

        ResponseEntity<List<Product>> response = productController.getProductsForInvestor(1);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(2, response.getBody().size());
    }
    
    private Investor mockInvestor() {

        return Investor.builder()
                .investorId(1)
                .emailAddress("sfmakgato@gmail.com")
                .dateOfBirth(LocalDate.of(1921, 12, 12))
                .firstName("France")
                .mobileNumber("0764730789")
                .lastName("Makgato")
                .address("123 Mokomene 0811")
                .enabled(true)
                .locked(false)
                .password("password")
                .userRole(UserRole.builder().roleName(Role.ADMIN_ROLE.toString()).build())
                .build();
    }

    private List<Product> mockProducts() {

        Product savingsProduct = Product.builder()
                .productID(2)
                .productType(ProductType.SAVINGS.toString())
                .currentBalance(36000.00)
                .productName("SavingsProduct")
                .investor(mockInvestor())
                .build();

        Product retirementProduct = Product.builder()
                .productID(1)
                .productType(ProductType.RETIREMENT.toString())
                .currentBalance(500000.00)
                .productName("RetirementProduct")
                .investor(mockInvestor())
                .build();

        List<Product> products = new ArrayList<>();
        products.add(savingsProduct);
        products.add(retirementProduct);

        return products;
    }
}
