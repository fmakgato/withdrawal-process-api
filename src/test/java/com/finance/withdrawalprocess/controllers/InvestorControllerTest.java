package com.finance.withdrawalprocess.controllers;

import com.finance.withdrawalprocess.entity.Investor;
import com.finance.withdrawalprocess.entity.UserRole;
import com.finance.withdrawalprocess.enums.Role;
import com.finance.withdrawalprocess.repository.InvestorRepository;
import com.finance.withdrawalprocess.services.InvestorService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
class InvestorControllerTest {

    @Mock
    InvestorService investorService;
    
    @Mock
    InvestorRepository investorRepository;

    @InjectMocks
    InvestorController investorController;


    @Sql(executionPhase= Sql.ExecutionPhase.BEFORE_TEST_METHOD,scripts = {
            "classpath:/data/drop-tables.sql",
            "classpath:/data/create-tables.sql"})
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = {
            "classpath:/data/drop-tables.sql"})
    @BeforeEach
    void setUp() {
        investorController = new InvestorController(investorService);
    }

    @Test
    void retrieveInvestorInformation() {
        Mockito.when(investorService.retrieveInvestorInformation(1)).thenReturn(mockInvestor());
        Mockito.when(investorRepository.findByInvestorId(1)).thenReturn(mockInvestor());

        ResponseEntity<Investor> response = investorController.retrieveInvestorInformation(1);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals("France", response.getBody().getFirstName());
    }
    
    private Investor mockInvestor() {

        return Investor.builder()
                .investorId(1)
                .emailAddress("sfmakgato@gmail.com")
                .dateOfBirth(LocalDate.of(1921, 12, 12))
                .firstName("France")
                .mobileNumber("0764730789")
                .lastName("Makgato")
                .address("123 Mokomene 0811")
                .enabled(true)
                .locked(false)
                .password("password")
                .userRole(UserRole.builder().roleName(Role.ADMIN_ROLE.toString()).build())
                .build();
    }
}
